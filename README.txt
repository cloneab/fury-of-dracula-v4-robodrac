title:  variant player, same game.  Replacing Dracula somewhat with a computer program.

Look um.. I wrote robodrac.py.  It is a python3 program that uses TK.  It is not perfect.  It won't be.  I wrote it for ME, not you.

But that doesn't mean you cannot use it.   

What is it?  You have the Fury of Dracula boardgame.  No one wants to play Dracula.  Or maybe you are just alone.
You can't play it because you need to hide tons of information from yourself.  
Well.... have the computer play Dracula.

I wrote that is a bit buggy because I kept having small problems.  But I think it works now.  I certainly have been able to play a few games fully with it.

Stuff that is missing.... Encounter cards.  Combat.  Rules I don't remember.  
I will not make it a full computer game.  

So how do you hide encounters from yourself?  Place the card face down.  It's not in this program.
How do you hide Dracula's combat cards?  Draw randomly.  It's not in this program.
How do you hide the trail?   OH!!! that's here.

In the program, there is a section called "Brains".  In there you move little tokens to advise Dracula without knowing what he is doing.

If I would talk to myself while play the game alone it would sound crazy.  But by typing this, you can see how the program would work.

"No no... Dracula, you only have 4 hit points left.  Going to the water is a DUMB move."
   Drag "water" token from "OK" box to "Dumb" box.
"That is crazy Draccy-poo.  You have 2 hit points left.  You CANNOT go to water.  It would end the game."
   Drag "water" token from "Dumb" box to "Disallowed" box.
"Hey.... It's a good idea to run Dark-Call now.  You will get a lot more encounter cards.
   Drag "Dark" from "OK" box to "Great" box.
"Hey Drac, you cannot go to Rome.  Dr Seward forbids it."
   Drag "blockspot" from Command box to "Rome".  (poof.  a "X-Rome" shows up in a Forbidden box).

Oh don't worry.  I did add some help to the zip file.  Some pictures.  I didn't make a video though.  I would need to write a program called "robocameraman" for that.  

I hope you like it.   

steve post  October 9, 2019

ps: February 1, 2020
I see no one downloads it.  No one likes it?   
But there was an article that says that many people want to play games solitaire.
So what messes it up?
A human playing a game solo has a stigma of being some type of loser?
Well.  You better not tell people that manufacture puzzles about that.

Supposedly tk is part of python.   It's not on the linux distribution.  tk?  the graphics.  
I grow tired of websites lying.   That's actually why I kept python so basic.  Why bother using some super special whatever when it fails on you?  A good reason for heavy documentation on open source.

And maybe people will not put it in the correct directory.  Fine... I will have to make a text file to pull in the correct file that person wants.

People don't want to mess with python at all.   So I will have to make a exe version.  Of course it will only work on windows something or other.

If a person restores when there is no restore file?  Fine.  Put one in.   Nah... person can figure it out on their own.

