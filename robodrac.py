
#  steve post   October 6, 2019   10:39am eastern usa time.
#

# add a configuration file note the location of all the data files.
# this includes if the directories are with a \ (windows) or / (unix).
#
#  who knows?   maybe / and \ will be replaced with $@#%^#$#$#%$*@# 






# generic tk moving tokens on a map and reacting to it
import random
import re 
import tkinter
import os
from tkinter import messagebox

#brownred = '#ff3c3c'
brownred = 'pink'
gameboardyellow = '#cdd26f'
braingreen  = '#64ff8b'
paleorange = '#fe9854'

class  subtoken():
    # I want a very specific object for holding on the ship information
    def initialize(self):
        self.shape = ''  # rect, tri, oval1 polygon45 etc
        self.id = 0  # the canvas id
        self.dx = 0  # the offset from the main point
        self.dy = 0  # the offset from the main point
        self.ord = 0 # the order of drawing. 0 is the main point.

class  mtoken():
    # main token
    def initialize(self):
        self.name = ''  # the key in dict subtokenlist
        self.spot = ''  # where it is.  where it is going.
        self.ospot = '' # if present, where is was.

class  cardo():
    def initialize(self):
        self.name = ''  # the key in dict subtokenlist
        self.cardback = 'x'
        self.cardfrnt = 'text'
        self.showtext = 'x'
        self.spot = 'x'   # the location or pwrcard code


class  spoto():
    # the main way you know where you are on the gameboard
    def initialize(self):
        self.name = 'none'  # the main key to id this spot
        self.type = 'off'   # the type of spot on the board
        # types are: cmd for brn slt drc water land gray
        self.id = 0  # the canvas id of the rectangle
        self.xyxy = (0,0,0,0)  # x1 y1 x2 y2
        self.title = ' '  # 
        self.color = 'gray' # 
        self.special = ''   # a special thing for the spot?
 
spotlist = {} 
weight = {}


cfiles = {}
cfiles['layout'] = 'robo_layout.txt'
cfiles['water' ] = 'robo__water.txt'
cfiles['c3'    ] = 'robo_____c3.txt'
cfiles['weight'] = 'robo_weight.txt'
cfiles['map'   ] = 'robo____map.txt'
cfiles['region'] = 'robo_region.txt'

# no enc because I skipped encounters
# no mina because I use region
# no cities  because it is pulled from c3


f=open("robo_config.txt")
f1 = f.readlines()
f.close()


for line in f1:
    if (re.search(r'#', line, flags=re.IGNORECASE )):
        continue
    elif ( len( line.split() ) <  2 ):
        continue
    else:
        line = line.replace("\n","" )
        filekey, filename, *trash = line.split() 
        if ( filekey in ( cfiles.keys() ) ):
            cfiles[filekey] = filename
for x in ( cfiles.keys() ):
    #print ("cfile  {} has is at  {}".format( x, cfiles[x] ))
    if ( os.path.isfile(  cfiles[x] )):
        print ("cfile  {} has is at  {}".format( x, cfiles[x] ))
    else:
        print(" Ye Olde BOMBEROO \n")
        print ("cfile  {} {} is not found or is not a file.".format( x, cfiles[x] ))
        exit()



f=open( cfiles['layout'], "r")
fl = f.readlines() # readlines reads the individual lines into a list
f.close()
y = -1
x = -1
for r in fl:
    y += 1
    tmpA = r.split()
    x = -1 
    startx = 5
    starty = 20
    widx = 75
    widy = 35
    for c in tmpA:
        x +=1
        if ( c == 'xxx' ):
           tmpkey = "{}-{}".format(x, y)
           spotlist[ tmpkey ] = spoto()
           spotlist[ tmpkey ].name = "{}-{}".format(x, y)
           spotlist[ tmpkey ].color = 'gray'
           spotlist[ tmpkey ].type = 'off'
           spotlist[ tmpkey ].title = ' '
        else:
           tmpkey = c
           spotlist[ tmpkey ] = spoto()
           spotlist[ tmpkey ].name = c
           spotlist[ tmpkey ].color = gameboardyellow
           spotlist[ tmpkey ].type = 'land'
           spotlist[ tmpkey ].title = ' '
        spotlist[ tmpkey ].xyxy = ( 
            (startx + (x * widx )),  (starty + (y * widy )),
            (startx + ((x+1) * widx )), (starty + ((y+1) * widy )) )
 
spotlist[ 'offboard' ] = spoto()
spotlist[ 'offboard' ].name = 'offboard'
spotlist[ 'offboard' ].color = 'gray'
spotlist[ 'offboard' ].xyxy = ( 880,400,950,500 )
spotlist[ 'offboard' ].title = ' '
spotlist[ 'offboard' ].type = 'off'

# the "layout for the control boxes below the locations
f2 = [
'cmdbox1 forbox1 brnbox1 sltbox1 drcbox1',
'cmdbox2 forbox2 brnbox2 sltbox2 drcbox2',
'cmdbox3 forbox3 brnbox3 sltbox3 drcbox3',
'cmdbox4 forbox4 brnbox4 sltbox4 drcbox4',
'cmdbox5 forbox5 brnbox5 sltbox5 drcbox5',
'cmdbox6 forbox6 brnbox6 sltbox6 drcbox6' ]

x=-1
y=-1
for r in f2:
    y += 1
    tmpA = r.split()
    x = -1 
    startx = 5
    starty = 380
    widx = 165
    widy = 50
    for c in tmpA:
        x +=1
        if ( c == 'xxx' ):
           tmpkey = "{}-{}".format(x, y)
           spotlist[ tmpkey ] = spoto()
           spotlist[ tmpkey ].name = "{}-{}".format(x, y)
           spotlist[ tmpkey ].color = 'gray'
           spotlist[ tmpkey ].type = 'off'
           spotlist[ tmpkey ].title = ' '
        else:
           tmpkey = c
           spotlist[ tmpkey ] = spoto()
           spotlist[ tmpkey ].name = c
           spotlist[ tmpkey ].color = paleorange
           spotlist[ tmpkey ].type = c[0:3] #cmd for brn slt drc 
           spotlist[ tmpkey ].title = ' '
        spotlist[ tmpkey ].xyxy = ( 
            (startx + (x * widx )),  (starty + (y * widy )),
            (startx + ((x+1) * widx )), (starty + ((y+1) * widy )) )

spotlist['brnbox1' ].color = braingreen
spotlist['brnbox2' ].color = braingreen
spotlist['brnbox3' ].color = braingreen
spotlist['brnbox4' ].color = braingreen
spotlist['brnbox5' ].color = braingreen
spotlist['brnbox6' ].color = braingreen
spotlist['sltbox1' ].color = brownred
spotlist['sltbox2' ].color = brownred
spotlist['sltbox3' ].color = brownred
spotlist['sltbox4' ].color = brownred
spotlist['sltbox5' ].color = brownred
spotlist['sltbox6' ].color = brownred
spotlist['drcbox1' ].color = brownred
spotlist['drcbox2' ].color = brownred
spotlist['drcbox3' ].color = brownred
spotlist['drcbox4' ].color = brownred
spotlist['drcbox5' ].color = brownred
spotlist['drcbox6' ].color = brownred

f=open( cfiles['water'],"r")
fset = f.readlines() # readlines reads the individual lines into a list
f.close()
r1 = ''
for r in fset:
    #removing the carriage return
    r = r.replace("\n","" )
    if ( r1 == '' ):
        r1 = r
    else:
        spotlist[ r1 ].color = 'lightblue'
        spotlist[ r1 ].type = 'water'
        r1 = ''  

f=open( cfiles['c3'],"r")
fset = f.readlines() # readlines reads the individual lines into a list
f.close()
r1 = ''
for r in fset:
    r = r.replace("\n","" )
    if ( r1 == '' ):
        r1 = r
        #removing the carriage return
    else:
        spotlist[ r1 ].title = r
        r1 = ''  
#for x in ( c3_city.keys() ):
#    #print ("code {} is city {}".format( x, c3_city[x] ))

f=open( cfiles['weight'],"r")
fset = f.readlines() # readlines reads the individual lines into a list
f.close()
r1 = ''
for r in fset:
    r = r.replace("\n","" )
    if ( r1 == '' ):
        r1 = r
        #removing the carriage return
    else:
        weight[ r1 ] = int( r )
        r1 = ''  
#for x in ( weight.keys() ):
#    print ("code {} has weight {}".format( x, weight[x] ))






# add the command boxes to c3_city
spotlist['cmdbox1' ].title = 'Commands'
spotlist['forbox1' ].title = 'Forbidden Paths Spots'
spotlist['brnbox1' ].title = 'Brains'
spotlist['brnbox2' ].title = 'Great'
spotlist['brnbox3' ].title = 'good'
spotlist['brnbox4' ].title = 'Ok'
spotlist['brnbox5' ].title = 'Stupid'
spotlist['brnbox6' ].title = 'not allowed'
spotlist['sltbox1' ].title = 'Trail6'
spotlist['sltbox2' ].title = 'Trail5'
spotlist['sltbox3' ].title = 'Trail4'
spotlist['sltbox4' ].title = 'Trail3'
spotlist['sltbox5' ].title = 'Trail2'
spotlist['sltbox6' ].title = 'Trail1'
spotlist['drcbox1' ].title = 'Dracula'
spotlist['drcbox2' ].title = 'Lair A'
spotlist['drcbox3' ].title = 'Lair B'
spotlist['drcbox4' ].title = 'Lair C'

f=open( cfiles['map'], "r")
fset = f.readlines() # readlines reads the individual lines into a list
f.close()


map_MAP = {}

for r in fset:
    r = r.replace("\n","" )
    tmpA = r.split()
    map_MAP[  tmpA[0] ] = tmpA[1:]
# verified the map loaded properly
#for m in map_MAP.keys() :
#    print( "node {}".format(m), end = "----" )
#    for n in map_MAP[ m ] :
#        print( n, end = ' ' )
#    print( "\n")

# add weights to power cards
weight[ 'hide' ] = 3
weight[ 'feed' ] = 3
weight[ 'miss' ] = 3  # missdirect  called missdir in brains.
weight[ 'wolf' ] = 3
weight[ 'dark' ] = 3




# load regions for Mina Harkers special power
f=open( cfiles['region'], "r")
flines = f.readlines() # readlines reads the individual lines into a list
f.close()
regiongroup = {}  # key is one key city, value is list of all cities in region
regionspot = {} # key is one city, value is one key city that denotes the region
#    fyi. I call the regions "the region with X in it"
for r in flines:
    r = r.replace("\n","" )
    tmpA = r.split()
    regiongroup[  tmpA[0] ] = tmpA[:]
    for y in ( tmpA ):
        regionspot[ y ] = tmpA[0]

# test it. 
#print( "in regionspot, but not weight")
#for x in ( regionspot.keys() ):
#   if ( x not in weight.keys() ):
#      print( "Oh. missing {}".format( x ) )
#print( "in weight, but not regionspot")
#for x in ( weight.keys()  ):
#   if ( x not in regionspot.keys() ):
#      print( "Oh. missing {}".format( x )) 
#quit()
#exit


# a surprise.  Even though I am making a spoto object.
#   even though I am presetting a value to a field.
#   it is ignoring the preset. So I have to preset on my own.
#   within the block of code where the object was created.
"""
for s in ( spotlist.keys() ):
    #print( "s {} name {} title {} color {}".format( s,
    # spotlist[ s ].name, spotlist[ s ].title, spotlist[ s ].color ))
    print( "s {} name {} type {} title {} color {}".format( s,
     spotlist[ s ].name, spotlist[ s ].type, spotlist[ s ].title, spotlist[ s ].color ))
quit()
"""



#----------------------------------------------------------------
# now start the TK.
#----------------------------------------------------------------

class robodrac_tk(tkinter.Tk):

    def __init__(self,parent):
        tkinter.Tk.__init__(self,parent)
        self.parent = parent
        self.initialize()

    def initialize(self):
        # I want a very specific object for holding on the ship information
        # this will hold an array of it.
        #        key is the shipname, its main tag, 
        #        value is the object with a set of canvas objects (rect, triangle, ovals)

        # I don't know if this should be a global or not.
        self.subtokenlist = {}   # key is the name for the set of subtokens
                            # value is the list of subtoken objects
 
        self.mtokenlist = {}  # key is name and matches subtokens
                             # value is the mtoken object
                             # it updatas in clear_token, make_token

        self.ids_in_spot = {}  # key is spot id, value is a LIST of tokens
                               # it gets filled via align_tokens function
 
        self.chosentoken = 'none' # the tag of the chosen token
        self.cardinc = 0   # an incremental number to drop cards on screen
        self.spotlist = spotlist   # key is spotname, value is spot object 
         # draw and place the different token
        self.map_MAP = map_MAP  # key is a spot, value is a list of possible paths to take
        self.weight = weight     # key is location code value is integer num of paths
        self.regiongroup = regiongroup  # key is keycity, value is list of cities in that region
        self.regionspot = regionspot  # key is anycity, value is its keycity in that region
        self.chosentoken = 'none' # the tag of the chosen token
        self.spot_of_token = {}  # where are the tokens  k=token v=spot
        self.Dracspot = 'drcbox1'
        self.cardlist = {}  # key is the id of the token, value is the cardo object
        self.trail = {} # key is slot number 1 through 6, a b c
                        # value is location
        self.trailc = {} # key is slot number 1 through 6, a b c
                        # value is the token of the card. c####
        
 
        self.trail['1'] = ''
        self.trail['2'] = ''
        self.trail['3'] = ''
        self.trail['4'] = ''
        self.trail['5'] = ''
        self.trail['6'] = ''
        self.trail['A'] = ''
        self.trail['B'] = ''
        self.trail['C'] = ''
        self.trailc['1'] = ''
        self.trailc['2'] = ''
        self.trailc['3'] = ''
        self.trailc['4'] = ''
        self.trailc['5'] = ''
        self.trailc['6'] = ''
        self.trailc['A'] = ''
        self.trailc['B'] = ''
        self.trailc['C'] = ''
        
        self.slt2tr = {} 
        self.slt2tr[ 'sltbox6' ] = '1'
        self.slt2tr[ 'sltbox5' ] = '2'
        self.slt2tr[ 'sltbox4' ] = '3'
        self.slt2tr[ 'sltbox3' ] = '4'
        self.slt2tr[ 'sltbox2' ] = '5'
        self.slt2tr[ 'sltbox1' ] = '6'
        self.slt2tr[ 'drcbox2' ] = 'A'
        self.slt2tr[ 'drcbox3' ] = 'B'
        self.slt2tr[ 'drcbox4' ] = 'C'
 
        self.topframe = tkinter.Frame( )
        self.topframe.pack(side="top", fill="x", expand=1)
        self.btmframe = tkinter.Frame( )
        self.btmframe.pack(side="top", fill="x", expand=1)

        self.w = tkinter.Canvas(self.topframe,width=850, height=710)
        self.w.pack(side='left', fill="both", expand=1)
        #  these 3 are the guts of the drag and drop for ships
        self.w.bind("<Button-1>",        self.clicksomething)   # do something based on what got clicked
        self.w.bind("<B1-Motion>",       self.drag_token)       # drag chosen token around the screen
        self.w.bind("<ButtonRelease-1>", self.dropsomething)    # do something based on chosen and xy of mouse

        for s in ( spotlist.keys() ):
            (x1, y1, x2, y2 ) = spotlist[ s ].xyxy
            thetag = (s)
            spotlist[ s ].id = self.w.create_rectangle( x1, y1, x2, y2, fill=spotlist[s].color, tags=thetag )
            #print( spotlist[s].title )
#            spotlist[ s ].tid = self.w.create_text( (x1+35), (y1+6), text=spotlist[s].title, anchor='w', fill="black", tags=thetag )
            spotlist[ s ].tid = self.w.create_text( (x1+3), (y1+6), text=spotlist[s].title, anchor='w', fill="black", tags=thetag )
        # draw a label
        self.w.create_text(500,10,   text="Fury of Dracula Board Locations" )
        self.turnstring = tkinter.StringVar()
        #self.turnstring.set( "labelcalled turnstring"  )
        #self.turnLabel = tkinter.Label(self.btmframe, textvariable=self.turnstring)
        #self.turnLabel.pack( side="left", fill="both", expand=1 )

        self.bclr  = tkinter.Button(self.btmframe, text=u"Move Dracula", command=self.move_dracula )
        self.bclr.pack( side="left", fill="both", expand=1 )
        self.bsho  = tkinter.Button(self.btmframe, text=u"Drac 2 steps", command=self.move_dracula2 )
        self.bsho.pack( side="left", fill="both", expand=1 )
        self.bqui  = tkinter.Button(self.btmframe, text=u"SaveGame", command=self.savegame )
        self.bqui.pack( side="left", fill="both", expand=1 )
        self.bqui  = tkinter.Button(self.btmframe, text=u"RestoreGame", command=self.restoregame )
        self.bqui.pack( side="left", fill="both", expand=1 )
        self.bqui  = tkinter.Button(self.btmframe, text=u"Cleanup Trail", command=self.redotrail )
        self.bqui.pack( side="left", fill="both", expand=1 )
        self.bqui  = tkinter.Button(self.btmframe, text=u"Quit", command=self.Quit )
        self.bqui.pack( side="left", fill="both", expand=1 )

        # thing   
        #  cmdhome[token]= spot  (used to reset command tokens )
        #  forbidspot = list of forbidden spots
        #  forbidpath = (a,b) tuple of forbidden paths
        #  landlist = list of spots
        #  waterlist = list of spots
        #notyet  regionlist key spot  value region (used by some cards and Mina)
        self.forbidspot = {}  #key is f#### mtoken value is spot 
        self.forbidpath = {}  #key is f#### mtoken value is tuple ( path1, path2 )
        self.landlist = []
        self.waterlist = []
        for x in ( self.spotlist.keys() ):
            if ( self.spotlist[ x ].type == 'land' ):
                self.landlist.append( x )
            if ( self.spotlist[ x ].type == 'water' ):
                self.waterlist.append( x )
     


        self.cmdhome = {}
        self.cmdhome['h1'   ] = 'san'             
        self.cmdhome['h2'   ] = 'sjo'              
        self.cmdhome['h3'   ] = 'lon'              
        self.cmdhome['h4'   ] = 'par'
        self.cmdhome['D'    ] = 'drcbox1'           
        self.cmdhome['path1'] = 'cmdbox3'      
        self.cmdhome['path2'] = 'cmdbox4'      
        self.cmdhome['h1-b' ] = 'brnbox5'       
        self.cmdhome['h2-b' ] = 'brnbox5'       
        self.cmdhome['h3-b' ] = 'brnbox5'       
        self.cmdhome['h4-b' ] = 'brnbox5'       
        self.cmdhome['water'] = 'brnbox5'      
        self.cmdhome['feed-b' ] = 'brnbox5'       
        self.cmdhome['dark-b' ] = 'brnbox4'       
        self.cmdhome['hide-b' ] = 'brnbox5'       
        self.cmdhome['wolf-b' ] = 'brnbox5'   
        self.cmdhome['miss-b' ] =  'brnbox5'    
        self.cmdhome['flip' ] = 'cmdbox1' 
        self.cmdhome['peek' ] = 'cmdbox2' 
        self.cmdhome['region_yn'   ] = 'cmdbox5'
        self.cmdhome['adjacent_yn'   ] = 'cmdbox5'
        self.cmdhome['remove'   ] = 'cmdbox2'
        self.cmdhome['castle'   ] =  'brnbox5'     
        self.cmdhome['blockspot'] =  'cmdbox5'   
           # very special tokens 
        self.cmdhome['Wolf'   ] =  'offboard'     
        self.cmdhome['Misdirect'   ] =  'offboard'     
        
        for tname in ( self.cmdhome.keys()  ):
            if ( tname == 'miss-b' ):
                self.subtokenlist[ tname ] = self.make_token( tname, self.cmdhome[ tname ], "Misdirect" )
            else:
                self.subtokenlist[ tname ] = self.make_token( tname, self.cmdhome[ tname ] )
        self.align_tokens()
        return


    def Quit(self):
   
        longwinded = """
Ok you are quiting this game.

This text only applies is you are quit in the middle of the game and will be restarting later on.

After you get out, copy Drac_diary.txt to a different name.  Do not look at its contents, just copy it.
Why?  Because the next time you open this file, that Drac_diary.txt file will be wiped out.
Oh and speaking of destruction, you did save your in progress game right?  If you are in the middle of the board game and are quitting, you should save it.  That way the trail in the program will match what is on the table.
"""
        ##messagebox.showinfo("For Your Information", longwinded)





        answer = messagebox.askyesno("quitting", "Are you sure you want to quit?")
        if  answer:
            quit()
        else:
            return

    def savegame( self ):
        # self.mtokenlist[tokenname]
        # self.subtokenlist[tokenname]
        # self.cardlist[c####]
        # self.spotlist[spot]
        # self.ids_in_spot[spot]
        # self.Dracspot
        # self.trail
        # self.trailc

        # output text
        #mtoken   mtokenlist    key  mtoken  name spot ospot
        #subtokenlist ..it is rebuild via create_token
        #cardo    cardlist      key  spot... the rest is calculated  name cardback cardfrnt showtext spot
        # cardback and showtext = spot.type  cardfrnt = spot.title  
        # Dracspot      locationcode
        # trail         key  locationcode
        # trailc        key  c####

        # example.....
        # output text
        # mtokenlist    h1-b h1-b brnbox5 brnbox5
        # mtokenlist    h1 h1 lon
        # cardlist      c0010  le_ 
        # cardlist      c0005  par 
        # Dracspot      kla
        # trail         1 le_
        # trail         A par
        # trailc        1 c0010
        # trailc        A c0005
        #print("savegame")

        longwinded = """
Ok you are saving this game.

After you get out, copy Drac_diary.txt to a different name.  Do not look at its contents, just copy it.
Why?  Because the next time you open this file, that Drac_diary.txt file will be wiped out.
Oh and speaking of destruction, you do realize that the robodrac_save.txt is about to be overwritten right?
I would assume if this is your second save, you copied the current robodrac_save.txt file to a different name.
"""
        #messagebox.showinfo("For Your Information", longwinded)


        answer = messagebox.askyesno("quitting", "Are you sure you want to save?")
        if  ( not answer ):
            messagebox.showinfo("whew","OK... you did not save.")
            return


        # Open a file for writing and create it if it doesn't exist
        f = open("robodrac_save.txt","w+")
  
        for x in ( self.mtokenlist.keys()):
            f.write("mtokenlist {} {} {} {}\n".format(
                x, self.mtokenlist[x].name, self.mtokenlist[x].spot, self.mtokenlist[x].ospot))

        for x in ( self.cardlist.keys()):
            f.write("cardlist {} {}\n".format(x, self.cardlist[x].spot ))
        f.write("Dracspot {}\n".format( self.Dracspot ))
        for x in ( self.trail.keys()):
            f.write("trail {} {}\n".format(x, self.trail[x] ))
        for x in ( self.trailc.keys()):
            f.write("trailc {} {}\n".format(x, self.trailc[x] ))
        for x in ( self.forbidspot.keys() ):
            f.write("forbidspot {} {}\n".format( x, self.forbidspot[x] ))
        for x in ( self.forbidpath.keys() ):
            (p1, p2) = ( self.forbidpath[ x ] )
            f.write("forbidpath {} {} {}\n".format( x, p1, p2 ))
        # close the file when done
        f.close()
        return  
        
    def restoregame( self ):
        if ( not os.path.isfile( "robodrac_save.txt" )):
            messagebox.showinfo("no save file", "Robodrac program is looking for\nfile robodrac_save.txt\nin this directory")
            return
        # 1. go through mtokenlist and self.clear_token( ALL OF THEM )
        # 2. as we get new tokens, from the save file, create them
        # 3. as we get proper text for cards, update those tokens.

        deletetokenlist = []
        deletetokenlist = list( self.mtokenlist.keys() )

        # every movable token ...it goes AWAY.
        for x in ( deletetokenlist ):
            self.clear_token( x )
        self.subtokenlist = {}
        self.cardlist = {}
        self.trail   = {}
        self.trailc  = {}
         

        r_forbidspot = []
        r_forbidpath = []
        self.cardinc = 0   # an incremental number to drop cards on screen
        old2newcard = {}   # use to convert old savegame cards to number card numbers
        old2cardspot = {}   # use to put new cards in their proper spots
 
        with open("robodrac_save.txt",'r') as f:
            for line in f:
                x = line.replace("\n","" )
                #print( x )
                lineA = x.split()
                if ( lineA[0] == 'mtokenlist' ):
                    self.subtokenlist[ lineA[1] ] = self.make_token( lineA[1], lineA[3] )
                if ( lineA[0] == 'Dracspot' ):
                    self.Dracspot = lineA[1]
                    
                if ( lineA[0] == 'forbidspot' ):
                    if ( len(lineA) == 3 ):
                        r_forbidspot.append( lineA[2] )
                if ( lineA[0] == 'forbidpath' ):
                    if ( len(lineA) == 4 ):
                        verytmpvar = ( lineA[2], lineA[3] )
                        r_forbidpath.append( verytmpvar )
                    
                if ( lineA[0] == 'cardlist' ):
                    self.cardinc += 1
                    mynewcard = "c{:0>4d}".format( self.cardinc )
                    old2newcard[ lineA[ 1 ] ] = mynewcard

                    #print(" from save card {} to new card {}".format( lineA[1], mynewcard ))

                    self.cardlist[ mynewcard ] = cardo()
                    self.cardlist[ mynewcard ].spot = lineA[2]
                    if ( lineA[2] in ('feed','wolf','dark','miss')):
                        self.cardlist[ mynewcard ].cardback = lineA[2]
                        self.cardlist[ mynewcard ].showtext = lineA[2]
                        self.cardlist[ mynewcard ].cardfrnt = lineA[2]
                    elif ( lineA[2] == 'hide'):
                        self.cardlist[ mynewcard ].cardback = 'land'
                        self.cardlist[ mynewcard ].showtext = 'land'
                        self.cardlist[ mynewcard ].cardfrnt = 'hide'
                    else:
                        self.cardlist[ mynewcard ].cardback = self.spotlist[ lineA[2] ].type 
                        self.cardlist[ mynewcard ].showtext = self.spotlist[ lineA[2] ].type 
                        self.cardlist[ mynewcard ].cardfrnt = self.spotlist[ lineA[2] ].title
                if ( lineA[0] == 'trail' ):
                    if ( len(lineA) == 3 ):
                        # this trail slot has data  the location
                        self.trail[ lineA[1] ] = lineA[2]
                    else:
                        self.trail[ lineA[1] ] = ''
                if ( lineA[0] == 'trailc' ):
                    if ( len(lineA) == 3 ):
                        # this trailc slot has data the token id for the card
                        self.trailc[ lineA[1] ] = lineA[2]
                    else:
                        self.trailc[ lineA[1] ] = ''
        # fix the old card values in trailc with the newly minted cards
        # first remove the old but remember where they were placed
        for x in ( self.trailc.keys() ):
            myold = self.trailc[x]
            if ( myold != '' and myold in ( old2newcard.keys() )):
                old2cardspot[ myold ] = self.mtokenlist[ myold ].spot
                self.clear_token( myold )
        # fix the old card values in trailc with the newly minted cards
        # second add the new remembering where they placed as the old card numbers
        for x in ( self.trailc.keys() ):
            myold = self.trailc[x]
            if ( myold != '' and myold in ( old2newcard.keys() )):
                mynew = old2newcard[myold]
                self.trailc[ x  ] = mynew
                cspot = old2cardspot[ myold ]
                self.subtokenlist[ mynew ] = self.make_token( mynew, cspot, self.cardlist[mynew].showtext )

        # if there were any forbidden paths or cities, 
        #   clear the forbid boxes from the screen
        #   
        #  
        #print( r_forbidspot )
        #print( r_forbidpath )
        if ( len( r_forbidspot ) > 0 or  len( r_forbidpath ) > 0 ):
            # clear anything in the forbid boxes
            # the tokens are old with invalid f###'s
            for trashspot in ('forbox1', 'forbox2', 'forbox3', 'forbox4', 'forbox5', 'forbox6' ):
                #print("anything in {}?".format( trashspot))
                for t in ( list( self.mtokenlist.keys() ) ):
                    if ( self.mtokenlist[t].spot == trashspot ):
                        #print("remove token {} from {}".format( t, trashspot ))
                        self.clear_token( t )
  
            if ( len( r_forbidspot ) > 0 ):
                # we have forbidden spots to add. 
                for forbin in ( r_forbidspot ):
                    self.cardinc += 1
                    fcard = "f{:0>4d}".format(  self.cardinc  )
                    ftext = "X-{}".format( self.spotlist[forbin].title )
                    self.forbidspot[fcard] = forbin
                    self.subtokenlist[ fcard ] = self.make_token( fcard, 'forbox2', ftext )
            if ( len( r_forbidpath ) > 0 ):
                # we have forbidden paths to add. 
                for forpat in ( r_forbidpath ):
                    (p1, p2) = ( forpat )
                    self.cardinc += 1
                    fcard = "f{:0>4d}".format( self.cardinc )
                    ftext = "X-{}-{}".format( self.spotlist[p1].title, self.spotlist[ p2 ].title )
                    self.forbidpath[ fcard ] = ( p1, p2 )
                    self.subtokenlist[ fcard ] = self.make_token( fcard, 'forbox2', ftext )
         
        self.align_tokens()
        self.align_trail()
        self.align_tokens()

        # close the file when done
        #no no no... I did "with open... it is already closed. f.close()
        return

    def dracdiary( self, mytext):
        # Open a Draculas itty bitty cutzy wootzy diary...OF DOOM!
        f = open("drac_diary.txt","a")
        f.write( mytext )
        f.close()
        return

    def drag_token( self, event ):
        if ( self.chosentoken != 'none' ):
            self.move_token( self.chosentoken, event.x, event.y )
            #self.align_tokens()
        return

    def align_tokens( self ):
        #go through each spot and rebuild this global dict.
        self.ids_in_spot = {}
        # key is the spot and value is a list of tokens within that spot
        # preset every spot to a null list
        for spots in ( self.spotlist.keys() ):
            self.ids_in_spot[spots] = [] 
         
        # number of tokens that are inside a spot is figured out
        for t in ( self.mtokenlist.keys() ):
            if ( self.mtokenlist[t].spot in ( self.ids_in_spot.keys() )):
                self.ids_in_spot[ self.mtokenlist[t].spot ].append( t )
            else:
                print("DEAD ERROR {} is not a normal spot for token".format( self.mtokenlist[t].spot, t))
                quit()
        # mtokenlist key is the token
        # mtokenlist[x].spot is the spot the token is on.
        # self.ids_in_spot[spot] is a list of tokens on that spot
        # spotlist[spot] key is spot, value is spoto object

        for spots in (self.ids_in_spot.keys() ):
            if (  len( self.ids_in_spot[spots] ) > 0):
                counter = -1 
                count2 = 0
                for token in ( self.ids_in_spot[spots] ):
                    if ( self.spotlist[spots].type in ('cmd','for','brn','slt','drc')):
                        counter +=1
                        if (counter >3):
                            counter = 0
                            count2 += 1
                        (x1, y1, x2, y2 ) = self.spotlist[ spots].xyxy
                        self.move_token( token, (x1+5+(50*count2)), (y1 +15+ (10* counter)))
                    else:
                        counter +=1
                        if (counter >1):
                            counter = 0
                            count2 += 1
                        (x1, y1, x2, y2 ) = self.spotlist[ spots].xyxy
                        self.move_token( token, (x1+5+(22*count2)), (y1 +15+ (10* counter)))
        self.align_trail()
        return                
                        
    def align_trail( self ):
        for slt in ( self.slt2tr.keys() ):
            self.trail[ self.slt2tr[ slt] ] = ''
            self.trailc[ self.slt2tr[ slt] ] = ''
            for x in ( self.ids_in_spot[slt] ):
                if ( x in ( self.cardlist.keys() ) ):
                    self.trail[ self.slt2tr[ slt] ] = self.cardlist[x].spot
                    self.trailc[ self.slt2tr[ slt] ] = x
                else:
                    self.trail[ self.slt2tr[ slt] ] = ''
                    self.trailc[ self.slt2tr[ slt] ] = ''
            #print("slt {} tr {} trail {} trailc {}".format( 
            #    slt, self.slt2tr[ slt],
            #    self.trail[ self.slt2tr[ slt ] ], self.trailc[ self.slt2tr[ slt] ]))
        return                
  

    def clicksomething( self, event ):
        # is it a ship, a surface sector, or a pegboard
        whatisit = 'dontknow'
        tokenfound = ''
        spotfound = ''
        # choices are:  a token to move around 
        #               or a spot to place a token

        for i in self.w.find_withtag( 'current' ):
            # a tl canvas item with current is something under the mouse
            # for every one of these items found, pull out all of their tags.
            for it in self.w.gettags( i ):
                for tokentag in ( self.subtokenlist.keys() ):
                    if ( tokentag == it ):
                        whatisit = 'token'
                        tokenfound = tokentag
        if ( whatisit == 'dontknow' ):
            # so it's not a token. ok look at the board
            for  spot in ( self.spotlist.keys() ):
                (x1, y1, x2, y2 ) = self.spotlist[ spot ].xyxy
                if ( event.x > x1 and event.x < x2 and event.y > y1 and event.y < y2 ):
                    spotfound = spot
                    whatisit == 'spot'

        if ( tokenfound in self.subtokenlist.keys() ):
            sublist = []
            if ( self.chosentoken in self.subtokenlist.keys() ):
                sublist =  self.subtokenlist[ self.chosentoken ]
                for subtoken in ( sublist ):
                    if ( subtoken.shape == 'oval'):
                        self.w.itemconfig( subtoken.id, fill='yellow')
            self.chosentoken = tokenfound 
            sublist = []
            sublist =  self.subtokenlist[ self.chosentoken ]
            for subtoken in ( sublist ):
                if ( subtoken.shape == 'oval'):
                    self.w.itemconfig( subtoken.id, fill='pink')
        return
 
    def dropsomething(self, event):
        if ( self.chosentoken in self.mtokenlist.keys() ):
            orig_spot = self.mtokenlist[ self.chosentoken].spot
        else:
            orig_spot = 'none'
        huntermoved = 0
        
        if ( self.chosentoken in ( self.mtokenlist.keys() )):
            for  spot in ( self.spotlist.keys() ):
                (x1, y1, x2, y2 ) = self.spotlist[ spot ].xyxy
                if ( event.x > x1 and event.x < x2 and event.y > y1 and event.y < y2 ):
                    # at this point I have 
                    #     selected token ----  self.chosentoken
                    #     orignal spot ------  orig_spot=  self.spot_of_token[ self.chosentoken]
                    #     new spot             new_spot =  spot

                    if ( self.chosentoken == 'flip' and spot in ( self.slt2tr.keys() )):
                        for x in ( self.ids_in_spot[ spot ] ):
                            if x in ( self.cardlist.keys() ):
                                if ( self.cardlist[ x ].showtext == self.cardlist[ x ].cardfrnt ):
                                    self.cardlist[ x ].showtext = self.cardlist[ x ].cardback
                                else:
                                    self.cardlist[ x ].showtext = self.cardlist[ x ].cardfrnt
                                for y in ( self.subtokenlist[ x ] ):
                                    if (y.shape == 'text' ):
                                        self.w.itemconfig( y.id, text = self.cardlist[ x ].showtext )
                        # and put the flip token back                                
                        spot = self.cmdhome['flip']
                    elif ( self.chosentoken == 'remove' and spot in ( self.slt2tr.keys())):
                        locationtoremove = self.trail[  self.slt2tr[ spot ]  ]
                        for x in ( self.ids_in_spot[ spot ] ):
                            if x in ( self.cardlist.keys() ):
                                self.clear_token( x )
                                self.cardlist.pop(x) 
                        self.redotrail( locationtoremove )        
                        # and put the remove token back                                
                        spot = self.cmdhome['remove']
                    elif ( self.chosentoken in ('h1','h2','h3','h4') ):
                        huntermoved = 1  
                        #messagebox.showinfo("run command", "token {} from {} to {}".format(self.chosentoken, orig_spot, spot  ))
                    elif ( self.chosentoken == 'peek' ):
                        self.peek_paths( spot )    
                        spot = self.cmdhome['peek']
                    elif ( self.chosentoken == 'region_yn' ):
                        self.peek_paths( spot=spot, result='yesno', looking='region' )    
                        spot = self.cmdhome['region_yn']
                    elif ( self.chosentoken == 'adjacent_yn' ):
                        self.peek_paths( spot=spot, result='yesno', looking='adjacent' )    
                        spot = self.cmdhome['adjacent_yn']
                    elif ( self.chosentoken in ('path1','path2','blockspot')):
                        checkit = self.i_forbid_it(self.chosentoken, spot)
                        if ( checkit != 'keeponmap' ):
                            spot = self.cmdhome[ self.chosentoken ]
                    elif ( self.chosentoken == 'remove' and \
                     spot in ('forbox1','forbox2','forbox3','forbox4','forbox5','forbox6' ) ):
                        for y in ( self.ids_in_spot[ spot ] ):
                            for z in ( list(self.forbidspot.keys()) ):
                                if y in ( self.forbidspot ):
                                    self.clear_token( y )
                                    self.forbidspot.pop( y )
                        for y in ( self.ids_in_spot[ spot ] ):
                            for z in ( list(self.forbidpath.keys()  ) ):
                                #(p1, p2 ) = self.forbidpath[z]
                                #print( "delete path {} {}".format(self.spotlist[p1].title , self.spotlist[p2].title))
                                if y in ( self.forbidpath ):
                                    self.clear_token( y )
                                    self.forbidpath.pop( y )
                        spot = self.cmdhome['remove']
                    elif ( self.chosentoken in ('h1-b', 'h2-b', \
                   'h3-b', 'h4-b', 'water', 'dark-b', \
                   'hide-b', 'wolf-b', 'miss-b', 'castle','feed-b')):
                        if ( spot in  ( "brnbox2", "brnbox3", "brnbox4", "brnbox5", "brnbox6")):
                            self.spot_of_token[ self.chosentoken ] = spot
                            self.mtokenlist[ self.chosentoken ].ospot =self.mtokenlist[ self.chosentoken ].spot 
                            self.mtokenlist[ self.chosentoken ].spot = spot
                        else:
                            spot = orig_spot
                    self.mtokenlist[ self.chosentoken ].ospot =self.mtokenlist[ self.chosentoken ].spot 
                    self.mtokenlist[ self.chosentoken ].spot = spot
                    self.spot_of_token[ self.chosentoken ] = spot
                    self.align_tokens()
                    self.chosentoken = 'none'
                    if ( huntermoved == 1 ): 
                        self.cross_paths()    
        return

    def make_token( self, tokenname, spot, optiontext='none' ):
        # it will be make token
        tmplist_subtokens = []
        thetag = (tokenname)
        outcolor = 'blue'
        color = 'red'
        (nx, ny, nx2, ny2 ) = self.spotlist[ spot ].xyxy
        ovalid = self.w.create_rectangle( nx, ny, nx+7, ny+7,  outline=color, fill=color, tag=thetag)
        if ( optiontext == 'none' ):
            optiontext = tokenname
        textid = self.w.create_text( nx+10,ny,  text=str(optiontext), anchor = 'w', fill='black', tags=thetag )

        tmplist_subtokens.append( subtoken() )
        tmplist_subtokens[0].shape = 'rect'
        tmplist_subtokens[0].id    = ovalid
        tmplist_subtokens[0].dx    = 0 
        tmplist_subtokens[0].dy    = 0 
        tmplist_subtokens[0].ord   = 1
        tmplist_subtokens.append( subtoken() )
        tmplist_subtokens[1].shape = 'text'
        tmplist_subtokens[1].id    = textid
        tmplist_subtokens[1].dx    = 10
        tmplist_subtokens[1].dy    = 0
        tmplist_subtokens[1].ord   = 2

        self.spot_of_token[ tokenname ] = spot
        self.mtokenlist[ tokenname ] = mtoken()
        self.mtokenlist[ tokenname ].name = tokenname
        self.mtokenlist[ tokenname ].spot = spot
        self.mtokenlist[ tokenname ].ospot = spot
        return (tmplist_subtokens)
    
    def clear_token( self, thetag ):
        #  use deleteitem( item# )
        if ( thetag in self.subtokenlist.keys() ):
            for subtokens in ( self.subtokenlist[thetag] ):
                #print( self.w.coords( shipbits.id ))
                self.w.delete( subtokens.id )
            self.subtokenlist.pop(thetag)   
        if ( thetag in self.mtokenlist.keys() ):
            self.mtokenlist.pop(thetag) 
        return
     
    def move_token( self, thetag, nx, ny ):
        dx = 0
        dy = 0
        ox = 0
        oy = 0
        temparray = []
        if thetag in self.subtokenlist.keys():
            for subtokens in ( self.subtokenlist[thetag] ):
                #print( self.w.coords( shipbits.id ))
                temparray = self.w.coords( subtokens.id )
                (ox, oy ) = temparray[0:2]
                dx = nx - ox + subtokens.dx
                dy = ny - oy + subtokens.dy         
                self.w.move(   subtokens.id, dx, dy) 
                self.w.tag_raise( subtokens.id )
        return
        
    def runbrains( self, startspot, special='1' ):
        # startspot is the starting location of dracula
        # special is to let us add options
        # '2'? moving two steps is possible
        

        
        diary = '-----------------------------------------------------\n'        
        nodelist = {} # set of possible movements
        nodepick = [] # what I pick from for the next location
        dumbpick = [] # only use this list if nodepick is empty
        brainint = {} #key brain thing, val integer
                      # 0 means dumb and remove, -1 means stays out 
                      # higher numbers add more cards to nodepick
        spotint = {}  # convert brain int to all spots    
        misslist = {} # should I even consider the idea of missdirect?


        # I cannot get misdirect to ever work.   So add key and set it.
        if ( special == 'miss' ):
            diary += " --- Misdirect --- Misdirect ----\n"
        if ( special == 'wolf' ):
            diary += " ---  Wolf     ---  Wolf     ----\n"


        if ( startspot == 'restart' ):
            diary += "------------------------------------------------------------\n"
            diary += "Dracula first location being picked.\n\n"
            hlist = {}   
            for h in ( 'h1','h2','h3','h4'):            
                hlist[ self.mtokenlist['h1'].spot  ] = 1     
            for s in ( spotlist.keys() ):
                if ( spotlist[s].type == 'land' ):  
                    if ( not (  s in ( hlist.keys() ) ) ):
                        nodelist[s] = 1
       
                   
            for mradd in ( nodelist.keys() ):
                nodepick.append( mradd )
            #print( nodepick )            
            random.shuffle( nodepick )
            theanswer = nodepick.pop()
            if ( theanswer in self.spotlist.keys() ):
                diary += " PICK: {}\n\n".format( self.spotlist[theanswer].title )
            else:
                diary += " PICK: {}\n\n".format( theanswer )
            self.dracdiary( diary )
            return theanswer     
                   
                   
        else:
            if ( special == 'wolf' ):
                nodelist2 = {}
                diary += "Dracula going possibly 2 land spots instead of 1.\n"
                for x1 in ( self.map_MAP[ startspot ] ):
                    if ( spotlist[x1].type == 'land' ):  
                        nodelist[x1] = 1
                for n1 in ( nodelist.keys() ):
                    if ( spotlist[n1].type != 'land' ):
                        nodelist.pop( n1 )
                for n1 in ( nodelist.keys() ):
                    for n2 in ( self.map_MAP[ n1 ] ):
                        if ( spotlist[n2].type == 'land' ):
                            nodelist2[n2] = 1           
                for x1 in ( nodelist2.keys() ):
                    nodelist[x1] = 1
                ### DEBUG
                #print( "WOLF nodelist for 2 land steps: ", end = " ")
                #for x in (nodelist.keys() ):
                #    print( x, end = " ")
                #print( " ")
                ### DEBUG END

            elif ( special == 'miss' ):
                # forget all about the prep stuff.
                # make new nodelist = to the trail
                nodelist = {}
                for t in ( self.trail.keys() ):
                    s = self.trail[t]
                    if ( s in self.spotlist.keys() ):
                        if ( spotlist[s].type == 'land' or spotlist[s].type == 'water' ):
                            nodelist[ s ] = 1
            else:
                if ( startspot in ( self.spotlist.keys())):
                    diary += "Dracula is at {}\n".format( self.spotlist[ startspot ].title )
                else:
                    diary += "Dracula at weird spot {}\n".format( startspot )
                for x1 in ( self.map_MAP[ startspot ] ):
                    nodelist[x1] = 1
        
        diary +="   Possible non-power locations: "
        for x in ( nodelist.keys() ):
            if ( x in self.spotlist.keys() ):
                diary += " {}".format( self.spotlist[ x ].title )
            else:
                diary += " {}".format( x )            
        diary += "\n"

        # make the brainint
        brainitems = ('h1-b','h2-b','h3-b','h4-b',
             'castle','dark-b','feed-b','hide-b','miss-b',
             'water','wolf-b')   
        brainlevels = {}
        brainlevels['brnbox2'] = 10 # Great
        brainlevels['brnbox3'] =  3 # Good
        brainlevels['brnbox4'] =  1 # Ok
        brainlevels['brnbox5'] =  0 # Stupid
        brainlevels['brnbox6'] = -1 # not allowed
        for BrainI in ( brainitems ):
            for BrainL in (brainlevels.keys() ):
                if ( self.spot_of_token[ BrainI ] == BrainL ):
                    brainint[ BrainI ] = brainlevels[ BrainL ]
        
        if ( startspot == 'restart' ):
            brainint['dark-b'] = -1
            brainint['feed-b'] = -1
            brainint['h1-b'] = 0
            brainint['h2-b'] = 0
            brainint['h3-b'] = 0
            brainint['h4-b'] = 0
            brainint['wolf-b'] = -1
            brainint['miss-b'] = -1
            brainint['hide-b'] = -1
        # work on the powers
        # note that spotint has no power cards in it yet.
        # note that nodelist has not power cards in it either.
        # trailpower
        # dark, hide, wolf, feed, miss
        
         
        #  build spotlistint key is spot, value is number of cards
        # only use land and water spots. (calc water too)
        #  oh... soon I will add action spots: dark, hide, feed, etc
        for s in ( spotlist.keys() ):
            if ( spotlist[s].type == 'land' ):  
                spotint[s] = 1
            if ( spotlist[s].type == 'water' ):
                spotint[s] = brainint[ 'water' ]
        
        # add powers
        if ( special != 'miss' and special != 'wolf' ):
            for pwr in ( 'hide','feed','miss','wolf','dark'):
                spotint[ pwr ] = 0
                nodelist[ pwr ] = 1

        # now apply the brains to the list of land and water locations
        for s in ( spotint.keys() ):
            spotint[s] = 12

        if ( special != 'miss' ):
            # remove the trail
            for n in ( list( nodelist.keys() ) ):
                for t in ( self.trail.keys() ):
                    #print(" HEY is node {} equal to trail {}".format( n, self.trail[t] ))
                    if ( n  == self.trail[t] ):
                        #print("due to trail, then remove {} from nodelist".format( n ))
                        if ( n in nodelist.keys() ):
                            nodelist.pop( n )

        for s in ( spotint.keys() ):
            # powers in the brain
            if ( s ==  'hide' ):
                spotint[s] = brainint['hide-b']
            if ( s ==  'feed' ):
                spotint[s] = brainint['feed-b']
            if ( s ==  'miss' ):
                spotint[s] = brainint['miss-b']
            if ( s ==  'wolf' ):
                spotint[s] = brainint['wolf-b']
            if ( s ==  'dark' ):
                spotint[s] = brainint['dark-b']
            # hunters on the brain
            if ( s == self.spot_of_token[ 'h1' ] ):
                if (  spotint[s] > brainint['h1-b'] and spotlist[s].type != 'water' ):
                    spotint[s] = brainint[ 'h1-b' ]
            if ( s == self.spot_of_token[ 'h2' ] ):
                if (  spotint[s] > brainint['h2-b'] and spotlist[s].type != 'water' ):
                    spotint[s] = brainint[ 'h2-b' ]
            if ( s == self.spot_of_token[ 'h3' ] ):
                if (  spotint[s] > brainint['h3-b'] and spotlist[s].type != 'water' ):
                    spotint[s] = brainint[ 'h3-b' ]
            if ( s == self.spot_of_token[ 'h4' ] ):
                if (  spotint[s] > brainint['h4-b'] and spotlist[s].type != 'water' ):
                    spotint[s] = brainint[ 'h4-b' ]
            # water on the brain
            if ( s in ( spotlist.keys() )):
                if ( spotlist[s].type == 'water' ):
                    if (  spotint[s] > brainint['water'] ):
                        spotint[s] = brainint[ 'water' ]
        # reset untouched spots from preset of 12 back to 1                    
        for s in ( spotint.keys()):
            if (spotint[s] > 11 ):
                #print("spotint for {} is a level {} so lower to 1".format(s,spotint[s]))
                spotint[s] = 1
        
        # add weights. these are the number of paths for each node
        # but.... spotint[s] is a multiplier!!!!! only use if >0
        for s in ( spotint.keys()):
            if ( spotint[s] > 0 ):
                spotint[s] = spotint[s] * self.weight[s]
 
        # this point, we will start to make the big list to pick from
        # nodelist is a dict (aka set) of nodes
        # spotint  is a dict  key of node, value is integer # of cards to add 
        # cards to add?  for... 
        # nodepick a list of cards to pull from. one of these cards
        # will be draculas location.


        # MORE TESTS forbidden stuff
        # but first, remove any nodes from nodelist if it is forbidden
        # I could have cycled through nodeint instead. but this is faster / cleaner
        
        # remove blockspots
        for s in (  list(nodelist.keys())  ):
            for f in ( self.forbidspot.keys() ):
                if ( self.forbidspot[f] == s ):
                    diary +="   remove blocked location: {}".format( s )
                    if ( n in nodelist.keys() ):
                        nodelist.pop( n )
         # remove blocked paths  path1-path2
        for s in (  list(nodelist.keys())  ):
            for f in ( self.forbidpath.keys() ):
                ( p1, p2 ) = ( self.forbidpath[f] )
                if ( p1 == self.Dracspot and p2 == s  ):
                    diary +="   remove blocked p1 to p2 path: {}".format( s )
                    if ( n in nodelist.keys() ):
                        nodelist.pop( n )
         # remove blocked paths  path2-path1
        for s in (  list(nodelist.keys())  ):
            for f in ( self.forbidpath.keys() ):
                ( p1, p2 ) = ( self.forbidpath[f] )
                if ( p1 == s  and p2 == self.Dracspot  ):
                    diary +="   remove blocked p2 to p1 path: {}".format( s )
                    if ( n in nodelist.keys() ):
                        nodelist.pop( n )
         # and remove completely disallowed locations
        for s in (  list(nodelist.keys())  ):
            if ( spotint[s] < 0 and s in list( nodelist.keys() )): 
                    diary +="   remove disallowed brains: {}".format( s )
                    if ( n in nodelist.keys() ):
                        nodelist.pop( n )

        # EVEN MORE TESTS FOR WOLF AND MISDIRECTION
        # if wolf pwer is in the list make sure it is still usable
        # there must be at least one land location within 2 steps or 
        # this is a waste to use, so remove it.
        # if wolf is available and len(nodelist) < 2 and len(nodelist2) > 2 
        #      wolf should be used
        #   at the moment  skip.

        #  PRETEST USING WOLF BLOCK start
        # if wolf is in use, you cannot use it until it is freed
        if ( self.mtokenlist['Wolf'].spot != 'offboard' ):
            if ( 'wolf' in nodelist.keys() ):
                nodelist.pop( 'wolf' )
        # if wolf would be useless... do not use it at all
        # pretend I am running wolf... call nodelist prewolf here.
        prewolf = {}
        for w1 in ( self.map_MAP[ startspot ] ):
            if ( spotlist[w1].type == 'land' ):
                prewolf[w1] = 1
        prewolf2 = {}
        for w1 in ( prewolf.keys() ):
            for w2 in ( self.map_MAP[ w1 ] ):
                if ( spotlist[w2].type == 'land' ):
                    prewolf2[w2] = 1
        for w2 in ( prewolf2.keys() ):
            prewolf[w2] = 1
        # now with prewolf filled with all 2 step land locations....        
        # again remove trail but for prewolf        
        for n in ( list( prewolf.keys() ) ):
            for t in ( self.trail.keys() ):
                if ( n  == self.trail[t] and n in ( prewolf.keys()) ):
                    prewolf.pop(n)
        # again remove forbidden
        for s in ( list( prewolf.keys() )   ):
            for f in ( self.forbidspot.keys() ):
                if ( self.forbidspot[f] == s and n in ( prewolf.keys())  ):
                    prewolf.pop( s )
        # remove blocked paths  path1-path2
        for s in (  list(prewolf.keys())  ):
            for f in ( self.forbidpath.keys() ):
                ( p1, p2 ) = ( self.forbidpath[f] )
                if ( p1 == self.Dracspot and p2 == s and n in ( prewolf.keys())  ):
                    prewolf.pop( s )
        # remove blocked paths  path2-path1
        for s in (  list(nodelist.keys())  ):
            for f in ( self.forbidpath.keys() ):
                ( p1, p2 ) = ( self.forbidpath[f] )
                if ( p1 == s  and p2 == self.Dracspot and n in ( prewolf.keys())   ):
                    prewolf.pop( s )
        # hunter are special in that we might be movig to attack
        # only avoid the hunters at level dumb or lower
        for s in ( list(prewolf.keys() ) ):
            for h in ( 'h1','h2','h3','h4'):
                if ( h == s ):
                    hspot = self.spot_of_token[ h ]
                    if (  spotint[hspot] < 1 and n in ( prewolf.keys())  ):
                        prewolf.pop( s )
        # powercards? not in this equation   
        if ( len( prewolf.keys() ) < 1 and 'wolf' in ( nodelist.keys()  )):
            nodelist.pop('wolf')
        #  PRETEST USING WOLF BLOCK end
        #################################

        ######## PRE MISS ###############
        ######## PRE MISS ###############
        ######## PRE MISS ###############
        if ( self.mtokenlist['Misdirect'].spot != 'offboard' ):
            if ( 'miss' in nodelist.keys() ):
                nodelist.pop( 'miss' )
        else:
            premiss = {}
            for t in ( self.trail.keys() ):
                s = self.trail[t]
                if ( s in self.spotlist.keys() ):
                    if ( spotlist[s].type == 'land' or spotlist[s].type == 'water' ):
                        premiss[ s ] = 1
            # again remove forbidden
            for s in ( list( premiss.keys() )   ):
                for f in ( self.forbidspot.keys() ):
                    if ( self.forbidspot[f] == s and n in ( premiss.keys()) ):
                        premiss.pop( s )
            # remove blocked paths  path1-path2
            for s in (  list(premiss.keys())  ):
                for f in ( self.forbidpath.keys() ):
                    ( p1, p2 ) = ( self.forbidpath[f] )
                    if ( p1 == self.Dracspot and p2 == s  and n in ( premiss.keys()) ):
                        premiss.pop( s )
            # remove blocked paths  path2-path1
            for s in (  list(nodelist.keys())  ):
                for f in ( self.forbidpath.keys() ):
                    ( p1, p2 ) = ( self.forbidpath[f] )
                    if ( p1 == s  and p2 == self.Dracspot  and n in ( premiss.keys()) ):
                        premiss.pop( s )
            # hunter are special in this we might be misdirecting to attack
            # only avoid the hunters at level dumb or lower
            for s in ( list(premiss.keys() ) ):
                for h in ( 'h1','h2','h3','h4'):
                    if ( h == s ):
                        hspot = self.spot_of_token[ h ]
                        if (  spotint[hspot] < 1   and n in ( premiss.keys())):
                            premiss.pop( s )
            
            if ( len( premiss.keys() ) < 1  and 'miss' in ( nodelist.keys() )):
                nodelist.pop('miss')
        #  PRETEST USING MISDIRECT BLOCK end
        ######## PRE MISS ###############
        ######## PRE MISS ###############
        ######## PRE MISS ###############

        # after sorting brains, I have a new list
        diary +="\n   All my choices: "
        for s in ( nodelist.keys() ):
            diary += s + " "
        diary += "\n"
        diary +="\n   Only the good choices: "
        for s in ( nodelist.keys() ):
            if ( spotint[s] > 0 ):
                diary += s + " "
        diary += "\n"

        # cycle through nodelist dict, making nodepick
        # also make dumbpick in case we have no choice but to be dumb
        for s in ( nodelist.keys() ):
            if (spotint[ s ] == 0  ):
                dumbpick.append( s )
            elif ( spotint[ s ] > 0 ):
                diary += " {}-{} ".format( s, spotint[s]) 
                for a in ( range(0, spotint[s] )):            
                    nodepick.append( s )

        diary += "\n"
         
        # if nodepick is empty, add dumbpick
        if (len(nodepick) < 1 ):
            nodepick = dumbpick
            diary +="I am in a corner. Add these dumb ideas: "
            for d in (dumbpick):
                diary += " {}".format( d )
            diary += "\n"    
        if (len(nodepick) < 1 ):
            messagebox.showinfo("ACK!","I am really stuck!\nSee Dracula Errors\nin Rules Reference." )
            diary += "I am really stuck.  I can't move.\n"
            self.dracdiary( diary )
            return startspot
             
        random.shuffle( nodepick )
        theanswer = nodepick.pop()


        if ( theanswer in self.spotlist.keys() ):
            diary += " PICK: {}\n".format( self.spotlist[theanswer].title )
        else:
            diary += " PICK: {}\n".format( theanswer )
        self.dracdiary( diary )
        
        if ( theanswer == 'wolf' ):
            diary += "Picked Wolf, so let us go through this AGAIN\n"
            messagebox.showinfo("Wolf","Playing the Wolf Card." )

            #print("WOLF: rerunning brains with WOLF option" )
 
            (theanswer, junktmp) = self.runbrains( self.Dracspot, 'wolf')
            if ( theanswer in self.spotlist.keys() ):
                diary += " PICK: {}\n".format( self.spotlist[theanswer].title )
            else:
                diary += " PICK: {}\n".format( theanswer )
            self.dracdiary( diary )

            #print("WOLF: answer is {}".format( theanswer ))
            return (theanswer, 'wolf')

	        #----- and end of the 'wolf' applied section
        if ( theanswer == 'miss' ):
            #print( "MISS: RE-running special runmissdir ")
		
            diary += "Picked MisDirect.  Let us go through this AGAIN\n"
            messagebox.showinfo("Misdirect","Playing the Misdirect Card." )
		
            (theanswer, junktext) = self.runbrains( self.Dracspot, 'miss')
		
            # identify the sltbox where the old card is.
            # remove the cards at that spot
            t2box = {}
            t2box['1'] = 'sltbox6' 
            t2box['2'] = 'sltbox5' 
            t2box['3'] = 'sltbox4' 
            t2box['4'] = 'sltbox3' 
            t2box['5'] = 'sltbox2' 
            t2box['6'] = 'sltbox1' 
            clearslot = ''
            for x in ( '1','2','3','4','5','6'):
                if ( self.trail[x] == theanswer ):
                    clearslot = t2box[ x ]
            if ( clearslot in ( self.ids_in_spot.keys() ) ):
                for x in ( self.ids_in_spot[ clearslot ] ):
                    if x in ( self.cardlist.keys() ):
                        self.clear_token( x )
                        self.cardlist.pop(x) 
		
            #print("MISS: answer is {}".format( theanswer ))
		
            # we need to remove the misdirect location from the trail
            self.redotrail( theanswer )           
		
            if ( theanswer in self.spotlist.keys() ):
                diary += " PICK: {}\n".format( self.spotlist[theanswer].title )
            else:
                diary += " PICK: {}\n".format( theanswer )
            self.dracdiary( diary )
            return (theanswer, 'miss')
 	        #----- and end of the 'miss' applied section
   
        return   (theanswer, 'norm')
        
    def move_dracula2(self):
        self.move_dracula( 2 )
        return
        
    def move_dracula( self, steps=1 ):
        #messagebox.showinfo("thinking...", "Robo Dracula moves." )
        underhood = 0
        ans_type = 'norm'
        if ( self.Dracspot == '' or self.Dracspot == 'drcbox1' ):
            steps = 1
            self.Dracspot = self.runbrains( 'restart' )
            #print("Dracula has been placed at city {} aka {}".format( self.Dracspot, self.spotlist[self.Dracspot].title ))
            self.cardinc += 1
            card = "c{:0>4d}".format( self.cardinc )
            self.cardlist[ card ] = cardo()
            self.cardlist[ card ].name = card
            self.cardlist[ card ].cardback = 'land'
            self.cardlist[ card ].cardfrnt = self.spotlist[  self.Dracspot ].title  
            self.cardlist[ card ].showtext = ''
            if ( underhood == 1 ):
                self.cardlist[ card ].showtext = 'land' + self.spotlist[ self.Dracspot ].title
            else:
                self.cardlist[ card ].showtext = 'land' 


            self.cardlist[ card ].spot = self.Dracspot
            self.subtokenlist[ card ] = self.make_token( card, 'sltbox6', self.cardlist[ card ].showtext )
            self.trail['1'] = self.Dracspot
            self.trailc['1'] = card



        else:
            olddrac = self.Dracspot
            (new_card, ans_type )  = self.runbrains( olddrac, steps )
 
            # if this was a power card, reset Dracspot, since he did not move
            if (new_card in ( 'hide','feed','miss','wolf','dark')):
                 self.Dracspot = olddrac
            else:
                 self.Dracspot = new_card
 
            # drop stuff in sltbox1 
            for x in ( self.ids_in_spot[ 'sltbox1' ] ):
                # need to "mature" cards at this point.
                mymessage = ''
                if ( self.mtokenlist['Wolf'].spot == 'sltbox1' ):
                    self.mtokenlist['Wolf'].spot = 'offboard' 
                if ( self.mtokenlist['Misdirect'].spot == 'sltbox1' ):
                    self.mtokenlist[ 'Misdirect'].spot = 'offboard' 

                if ( x in ( self.cardlist.keys() )):
                    if ( self.cardlist[x].spot == 'hide'):
                        mymessage = 'Remove Hide Power Card'
                    if ( self.cardlist[x].spot in ( self.spotlist.keys())):
                        if ( self.spotlist[ self.cardlist[x].spot ].type == 'land' ):
                            if ( self.trail['A'] == '' ):
                                mymessage = "Card can go to a Lair A"
                                self.trail['A'] = self.trail['6']
                                self.trailc['A'] = self.trailc['6']
                                self.mtokenlist[ x ].spot = 'drcbox2'
                            elif ( self.trail['B'] == '' ):
                                mymessage = "Card can go to a Lair B"
                                self.trail['B'] = self.trail['6']
                                self.trailc['B'] = self.trailc['6']
                                self.mtokenlist[ x ].spot = 'drcbox3'
                            elif ( self.trail['C'] == '' ):
                                mymessage = "Card can go to a Lair C"
                                self.trail['C'] = self.trail['6']
                                self.trailc['C'] = self.trailc['6']
                                self.mtokenlist[ x ].spot = 'drcbox4'
                            else:     
                                #mymessage = "Card {} goes away".format( self.cardlist[ x ].cardfrnt )
                                #mymessage = "Card goes away"
                                self.clear_token( x )
                                self.cardlist.pop( x )
                        else:     
                            #mymessage = "Card {} goes away".format( self.cardlist[ x ].cardfrnt )
                            self.clear_token( x )
                            self.cardlist.pop( x )
                    else:     
                        #mymessage = "Card {} goes away".format( self.cardlist[ x ].cardfrnt )
                        self.clear_token( x )
                        self.cardlist.pop( x )
                    self.trail['6'] = ''
                    self.trailc['6'] =  ''
                    if ( mymessage ): 
                        messagebox.showinfo("Mature", mymessage)
                
            # move slot 2>1 3>2 4>3 5>4 6>5
            for x in ( self.ids_in_spot[ 'sltbox2' ] ):
                self.mtokenlist[ x ].spot = 'sltbox1'
                self.trail['6'] = self.trail['5']
                self.trailc['6'] = self.trailc['5']
            for x in ( self.ids_in_spot[ 'sltbox3' ] ):
                self.mtokenlist[ x ].spot = 'sltbox2'
                self.trail['5'] = self.trail['4']
                self.trailc['5'] = self.trailc['4']
            for x in ( self.ids_in_spot[ 'sltbox4' ] ):
                self.mtokenlist[ x ].spot = 'sltbox3'
                self.trail['4'] = self.trail['3']
                self.trailc['4'] = self.trailc['3']
            for x in ( self.ids_in_spot[ 'sltbox5' ] ):
                self.mtokenlist[ x ].spot = 'sltbox4'
                self.trail['3'] = self.trail['2']
                self.trailc['3'] = self.trailc['2']
            for x in ( self.ids_in_spot[ 'sltbox6' ] ):
                self.mtokenlist[ x ].spot = 'sltbox5'
                self.trail['2'] = self.trail['1']
                self.trailc['2'] = self.trailc['1']

            # make new card and place in sltbox6
            self.cardinc += 1
            card = "c{:0>4d}".format( self.cardinc )
            self.cardlist[ card ] = cardo()
            self.cardlist[ card ].name = card
            self.cardlist[ card ].spot = new_card
            self.cardlist[ card ].cardback = ''
            self.cardlist[ card ].cardfrnt = ''
            self.cardlist[ card ].showtext = ''
          
            if (new_card in ( 'hide','feed','miss','wolf','dark')):
                if ( new_card == 'hide' ):
                    self.cardlist[ card ].cardback = 'land'
                    self.cardlist[ card ].cardfrnt = 'hide'
                    if ( underhood == 1 ):
                        self.cardlist[ card ].showtext = 'land-hide'
                    else:
                        self.cardlist[ card ].showtext = 'land'
                    
                if ( new_card   == 'dark' ):
                    self.cardlist[ card ].cardback = 'DarkCall'
                    self.cardlist[ card ].cardfrnt = 'DarkCall'
                    self.cardlist[ card ].showtext = 'DarkCall'
                if ( new_card   == 'feed' ):
                    self.cardlist[ card ].cardback = 'Feed'
                    self.cardlist[ card ].cardfrnt = 'Feed'
                    self.cardlist[ card ].showtext = 'Feed'
            else:
                if ( self.spotlist[  self.Dracspot ].type  == 'water' ):
                    self.cardlist[ card ].cardback = 'water'
                    self.cardlist[ card ].cardfrnt = self.spotlist[  self.Dracspot ].title  
                    if ( underhood == 1 ):
                        self.cardlist[ card ].showtext = 'water' + self.spotlist[ self.Dracspot ].title
                    else:
                        self.cardlist[ card ].showtext = 'water' 
                elif (   self.Dracspot  == 'cas' ):
                    self.cardlist[ card ].cardback = 'Castle Dracula'
                    self.cardlist[ card ].showtext = 'Castle Dracula'
                    self.cardlist[ card ].cardfrnt = 'Castle Dracula'
                else:
                    if ( underhood == 1 ):
                        self.cardlist[ card ].showtext = 'land' + self.spotlist[ self.Dracspot ].title
                    else:
                        self.cardlist[ card ].showtext = 'land' 
                    self.cardlist[ card ].cardback = 'land'
                    self.cardlist[ card ].cardfrnt = self.spotlist[  self.Dracspot ].title  
            self.subtokenlist[ card ] = self.make_token( card, 'sltbox6', self.cardlist[card].showtext )
            self.trail['1'] = str( self.Dracspot )  # yes. Dracula's location card might be powercard
            self.trailc['1'] = card
        self.mtokenlist[ 'D' ].spot = 'drcbox1'
        if ( ans_type == 'wolf' ):
            self.mtokenlist['Wolf'].spot = 'sltbox6'
        if ( ans_type == 'miss' ):
            self.mtokenlist['Misdirect'].spot = 'sltbox6'

        self.align_tokens()
        self.cross_paths()
        
        # write the trail locations into the diary
        diary = " --- the Trail --- \n"
        for t in ( '1','2','3','4','5','6','A','B','C'):
            tc = self.trailc[ t ]
            if ( tc in self.cardlist.keys() ):
                btmcard = self.cardlist[ tc ].cardfrnt 
                topcard = self.cardlist[ tc ].showtext 
            else:
                btmcard = tc
                topcard = tc
            diary +="{} {} {} {}\n".format( t, topcard, btmcard, tc )
        #print( diary )
        self.dracdiary( diary )
        return
    
    def i_forbid_it( self, token, spot ):
        #  cmdhome[token]= spot  (used to reset command tokens )
        #  forbidspot = list of forbidden spots (fcard, spot)
        #  forbidpath =  tuple of forbidden paths (fcard, a,b)
        #  landlist = list of spots
        #  waterlist = list of spots
        fcard = ''
        ftext = ''

        if ( not  token in ('blockspot','path1','path2')):
            return
        if ( not spot in (  self.landlist )):
            return
        if (token == 'blockspot' ):
            self.cardinc += 1
            fcard = "f{:0>4d}".format( self.cardinc )
            ftext = "X-{}".format( self.spotlist[spot].title )
            self.forbidspot[fcard] = spot
            self.subtokenlist[ fcard ] = self.make_token( fcard, 'forbox2', ftext )
        if (token == 'path1' ):
            if ( self.mtokenlist[ 'path2' ].spot in self.landlist ):
               self.cardinc += 1
               fcard = "f{:0>4d}".format( self.cardinc )
               #ftext = "X-{}-{}".format( spot, self.mtokenlist['path2'].spot )
               ftext = "X-{}-{}".format( self.spotlist[spot].title, self.spotlist[ self.mtokenlist['path2'].spot ].title )
               self.forbidpath[ fcard ] = ( spot, self.mtokenlist['path2'].spot )
               self.subtokenlist[ fcard ] = self.make_token( fcard, 'forbox2', ftext )
               self.mtokenlist[ 'path2' ].spot = self.cmdhome[ 'path2' ]
            else:
               return 'keeponmap'
        if (token == 'path2' ):
            if ( self.mtokenlist[ 'path1' ].spot in self.landlist ):
               self.cardinc += 1
               fcard = "f{:0>4d}".format( self.cardinc )
               #ftext = "X-{}-{}".format( spot, self.mtokenlist['path1'].spot )
               ftext = "X-{}-{}".format( self.spotlist[spot].title, self.spotlist[ self.mtokenlist['path1'].spot ].title )
               self.forbidpath[ fcard ] = ( spot, self.mtokenlist['path1'].spot )
               self.subtokenlist[ fcard ] = self.make_token( fcard, 'forbox2', ftext )
               self.mtokenlist[ 'path1' ].spot = self.cmdhome[ 'path1' ]
            else:
               return 'keeponmap'
        #print("Forbidden locations: ", end=" ")
        #for f in ( self.forbidspot.keys() ):
        #    print( "{}-{}".format( f, self.forbidspot[f]) , end=" ")
        #print(" ")
        #print("Forbidden paths: ", end=" ")
        #for f in ( self.forbidpath.keys() ):
        #    ( coming, going ) = ( self.forbidpath[f] )
        #    print( "{}-{}-{}".format( f, coming, going ) , end=" ")
        #print(" ")

        return
    
    def peek_paths( self, spot, result="reveal", looking="trail" ):
        # result options:  reveal means flip over a trail card, and tell user
        #                  yesno  means tell user either yes or no
        # looking options: trail means look at all trail locations
        #                  drac means just look at dracula's location
        #                  region means look at whole region
        #                  adj means all spots adjacent to requested spot

         
        mymessage =''
        if ( looking == 'drac' ):   
            if ( spot == self.Dracspot ): 
                if (result == 'reveal' ):
                    mymessage = "Found Dracula at {}!\n".format( self.spotlist[ self.Dracspot ].title ) 
                    for x in ( self.ids_in_spot[ trail2slot[ t ] ] ):
                        if x in ( self.cardlist.keys() ):
                            self.cardlist[ x ].showtext = self.cardlist[ x ].cardfrnt
                            for y in ( self.subtokenlist[ x ] ):
                                if (y.shape == 'text' ):
                                    self.w.itemconfig( y.id, text = self.cardlist[ x ].showtext )
                if (result == 'yesno' ):
                    mymessage = "Yes"    
            else:
                if (result == 'yesno' ):
                    mymessage = "no"    
        if ( looking == 'trail' ):   
            # part II.  check trail and flip over cards 
            trail2slot = {}
            trail2slot['1' ] = 'sltbox6'
            trail2slot['2' ] = 'sltbox5'
            trail2slot['3' ] = 'sltbox4'
            trail2slot['4' ] = 'sltbox3'
            trail2slot['5' ] = 'sltbox2'
            trail2slot['6' ] = 'sltbox1'
            trail2slot['A' ] = 'drcbox2'
            trail2slot['B' ] = 'drcbox3'
            trail2slot['C' ] = 'drcbox4'
            for t in ( '1','2','3','4','5','6','A','B','C' ):
                if ( spot == self.trail[t] ):
                    if ( result == 'reveal' ):
                        mymessage += "Found Trail {} at {}!\n".format(
                               t, self.spotlist[ self.trail[t] ].title )
                        # flip over any cards on that trail slot 
                        for x in ( self.ids_in_spot[ trail2slot[ t ] ] ):
                            if x in ( self.cardlist.keys() ):
                                self.cardlist[ x ].showtext = self.cardlist[ x ].cardfrnt
                                for y in ( self.subtokenlist[ x ] ):
                                    if (y.shape == 'text' ):
                                        self.w.itemconfig( y.id, text = self.cardlist[ x ].showtext )
                    if ( result == 'yesno' ):
                        mymessage == 'Yes' 
        if ( looking == 'region' and result == 'yesno' ):
            if ( spot in self.regionspot.keys()):
                regiontocheck = self.regionspot[ spot ]
                mymessage = "No.  Not in this Region" 
                for r in ( self.regiongroup[ regiontocheck ] ):
                    if ( r == self.Dracspot ):
                        mymessage = "Yes.  In this Region"    
            else:
                mymessage = "No.  That is not even a Region" 
        if ( looking == 'adjacent' and result == 'yesno' ):
            if ( spot in self.map_MAP.keys() ):
                adjlist = self.map_MAP[ spot  ]
                #print( "test adj yes no from spot {}".format( spot ))
                mymessage = "No.  Dracula is not adjacent to {}".format( self.spotlist[ spot ].title ) 
                for r in ( adjlist ):
                    #print(" is {} match Drac at {} ".format( r, self.Dracspot ))
                    if ( r == self.Dracspot and self.spotlist[ self.Dracspot ].type == 'land' ):
                        mymessage = "Yes.  Dracula is adjacent to {}".format(
                             self.spotlist[ spot ].title ) 
            
        if ( len(mymessage) > 0 ):
            messagebox.showinfo("From Checking Hidden Things", mymessage)
        return  
        
  
    def cross_paths( self ):
        # pretty simple.  
        # check if any hunter runs over any part of the trail.
        #  self.cardlist[ c####].code = a location maybe
        #   if a hunter hits it, flip over the card.
        # mtoken[ h1 h2 h3 h4 ].spot are where they are
        # if a hunter lands on same spot as self.Dracspot, move mtoken[drac]
        # part I.  drac meets someone.
        mymessage =''
        (x1, y1, x2, y2 ) = self.spotlist[self.Dracspot  ].xyxy
        x1 = x1 + 4
        y1 = y1 + 4
      
        if ( self.mtokenlist[ 'h1' ].spot == self.Dracspot and self.spotlist[ self.Dracspot ].type == 'land' ):
            mymessage = "Hunter 1 found Dracula at {}!\n".format( self.spotlist[ self.Dracspot ].title )
            self.move_token('D', x1, y1  )
        if ( self.mtokenlist[ 'h2' ].spot == self.Dracspot and self.spotlist[ self.Dracspot ].type == 'land' ):
            mymessage += "Hunter 2 found Dracula at {}!\n".format( self.spotlist[ self.Dracspot ].title )
            self.move_token('D', x1, y1 )
        if ( self.mtokenlist[ 'h3' ].spot == self.Dracspot and self.spotlist[ self.Dracspot ].type == 'land' ):
            mymessage += "Hunter 3 found Dracula at {}!\n".format( self.spotlist[ self.Dracspot ].title )
            self.move_token('D', x1, y1 )
        if ( self.mtokenlist[ 'h4' ].spot == self.Dracspot and self.spotlist[ self.Dracspot ].type == 'land' ):
            mymessage += "Hunter 4 found Dracula at {}!\n".format( self.spotlist[ self.Dracspot ].title )
            self.move_token('D', x1, y1 )

        # part II.  check trail and flip over cards 
        # trail1,2,3,4,5,6  Lair A B C
        # trail[6 ] self.ids_in_spot[ 'sltbox1' ][0].spot
        # trail[5 ] self.ids_in_spot[ 'sltbox2' ][0].spot
        # trail[4 ] self.ids_in_spot[ 'sltbox3' ][0].spot
        # trail[3 ] self.ids_in_spot[ 'sltbox4' ][0].spot
        # trail[2 ] self.ids_in_spot[ 'sltbox5' ][0].spot
        # trail[1 ] self.ids_in_spot[ 'sltbox6' ][0].spot
        trail2slot = {}
        trail2slot['1' ] = 'sltbox6'
        trail2slot['2' ] = 'sltbox5'
        trail2slot['3' ] = 'sltbox4'
        trail2slot['4' ] = 'sltbox3'
        trail2slot['5' ] = 'sltbox2'
        trail2slot['6' ] = 'sltbox1'
        trail2slot['A' ] = 'drcbox2'
        trail2slot['B' ] = 'drcbox3'
        trail2slot['C' ] = 'drcbox4'
        
        hunterlist = ( 'h1','h2','h3','h4')
        for t in ( '1','2','3','4','5','6','A','B','C' ):
            if ( self.trail[ t ] in self.spotlist.keys() ):
                if ( self.spotlist[ self.trail[  t  ]   ].type == 'land' ):
                    for h in (hunterlist):
                        if ( self.mtokenlist[ h ].spot == self.trail[t] ):
                            mymessage += "Hunter {} found Trail {} at {}!\n".format( h, t, self.spotlist[ self.trail[t] ].title )
                            # flip over any cards on that trail slot 
                            for x in ( self.ids_in_spot[ trail2slot[ t ] ] ):
                                if x in ( self.cardlist.keys() ):
                                    self.cardlist[ x ].showtext = self.cardlist[ x ].cardfrnt
                                    for y in ( self.subtokenlist[ x ] ):
                                        if (y.shape == 'text' ):
                                            self.w.itemconfig( y.id, text = self.cardlist[ x ].showtext )

        if ( len(mymessage) > 0 ):
            messagebox.showinfo("SURPRISE!", mymessage)
        return  

    def redotrail( self, theanswer='none' ):
        #  REDO THE TRAIL  trail['1'] = locationcode ex: par(Paris) 
        #                  trailc['1'] = card mtoken, ex: c0023
        # this is after something is removed.  or just redo it.
        newtrail = []
        newtrailc = []
        t2box = {}
        t2box['1'] = 'sltbox6' 
        t2box['2'] = 'sltbox5' 
        t2box['3'] = 'sltbox4' 
        t2box['4'] = 'sltbox3' 
        t2box['5'] = 'sltbox2' 
        t2box['6'] = 'sltbox1' 

        #print( "DEBUG: BEFORE -----")
        #for x in ( '1','2','3','4','5','6'):
        #    print( "{} {} {}".format( x, self.trail[x], self.trailc[x] ))

        for x in ( '1','2','3','4','5','6'):
            #print( "DEBUG: look at trail {} loc {} boardspot {}".format( x, self.trail[x], t2box[x] ))
            if ( self.trail[x] == theanswer ):
                #print( "DEBUG: clear trail {} because it matches answer {}".format(x,theanswer))
                if ( self.trailc[x] in list(self.trailc.keys()) ):
                    self.clear_token(  self.trailc[ x ] )
                #print("skip this trail {}".format(x))
            else:
                #print("DEBUG: append loc {} card {}".format( self.trail[x], self.trailc[x] ))  
                if( len( self.trail[x] ) > 0 ):
                    newtrail.append(self.trail[x])   
                    newtrailc.append(self.trailc[x])

        for x in ( '1','2','3','4','5','6'):
            self.trail.pop( x )
            self.trailc.pop( x )

        for x in ( range(0,6)):
            #print( "DEBUG: x {}  {}  length newtrail {}".format( x, (x + 1 ), len(newtrail)))
            if ( (x+1) <= len(newtrail) ):
                self.trail[ str( x+1 ) ] = newtrail[x] 
                self.trailc[ str( x+1 ) ] = newtrailc[x] 
            else:
                self.trail[ str( x+1 ) ] = ''
                self.trailc[ str( x+1 ) ] = '' 

        #print( "DEBUG: AFTER  -----")
        #for x in ( '1','2','3','4','5','6'):
        #    print( "{} {} {}".format( x, self.trail[x], self.trailc[x] ))

        # move the tokens now that the trailc has been fixed
        for x in ( '1','2','3','4','5','6'):
            if ( self.trailc[x] in ( self.mtokenlist.keys() )):
                self.mtokenlist[ self.trailc[x] ].spot = t2box[x]

        # make sure all the trail keys are properly present
        for x in ( '1','2','3','4','5','6','A','B','C'):
            if ( not x in ( self.trail.keys() ) ):
                self.trail[ x ] = ''
            if ( not x in ( self.trailc.keys() ) ):
                self.trailc[ x ] = ''

        # I am annoyed.  I have had it with this bugger.
        # sure.  it is all set up.  But the graphic tokens 
        # on the canvas?  They are all goobered up.
        #
        for x in ( '1','2','3','4','5','6'):
            if (self.trailc[x] in (  self.mtokenlist.keys() )):
                self.mtokenlist[  self.trailc[ x ] ].spot = t2box[ x ]
        

        goodcards = []
        for x in ( self.trailc.keys() ):
            if ( len( self.trailc[ x ] ) > 0 ):
                 goodcards.append( self.trailc[ x ] )
        goodcards.append('Wolf')
        goodcards.append('Misdirect')
        
        #print( goodcards )

        for c in ( self.cardlist.keys() ):
            #print( "c is {}".format( c ))
            if (  c not in (goodcards)  ):
                #print( "clear token {}".format( c ) )
                self.clear_token( c )

        #FYI:
        #self.subtokenlist[ card ] = self.make_token( card, 'sltbox6', self.cardlist[card].showtext )
        #self.trail['1'] = str( self.Dracspot )  # yes. Dracula's location card might be powercard
        #self.trailc['1'] = card
 
        self.align_trail()
        self.align_tokens()        

        return 


if __name__ == "__main__":

    # clear and create the dracula diary before starting.
    f = open("drac_diary.txt","a+")
    f.write( "------------ Starting or restarting the program -------------\n")
    f.write( "My Diary.  By Robotic Count Dracula\n" )
    f.write(  "    beep beep and stuff.\n")
    f.close()
 

    app = robodrac_tk(None)
    app.title('Robo Dracula')
    #app.geometry("830x710")
    app.mainloop()
